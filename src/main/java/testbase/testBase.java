package main.java.testbase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import main.java.waithelper.waitHelper;

public class testBase {

	public WebDriver driver;
	File file;
	FileReader reader;
	public Properties pr;
	Logger logger = Logger.getLogger(testBase.class);
	
	Calendar calendar = Calendar.getInstance();
	SimpleDateFormat formator = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
	
	ExtentReports extentreport = new ExtentReports(System.getProperty("user.dir")+"\\src\\main\\java\\reports\\test_" + formator.format(calendar.getTime())+".html");
	ExtentTest extenttest;
	
	public String readFromPropertyFile(String key) throws IOException
	{
		String log4j = "log4j.properties";
		PropertyConfigurator.configure(log4j);
		file = new File(System.getProperty("user.dir")+"\\src\\main\\java\\config\\config.properties");
		reader = new FileReader(file);
		pr = new Properties();
		pr.load(reader);
		logger.info("loading proprties file");
		return pr.getProperty(key);
	}
	
	public void launchBrower(String browserName)
	{
		if(browserName.equalsIgnoreCase("Chrome"))
		{
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		
		else if(browserName.equalsIgnoreCase("Chrome"))
		{
			driver = new FirefoxDriver();
		}
	}
	
	public String getScreenshot() throws IOException
	{
		TakesScreenshot screenshot = (TakesScreenshot)driver;
		File imagefile = screenshot.getScreenshotAs(OutputType.FILE);
		String imagelocation = System.getProperty("user.dir")+ "\\src\\main\\java\\screenshots\\screenshot_";
		String actualimagename = imagelocation + formator.format(calendar.getTime())+".png";
	    File imagefile1=new File(actualimagename);
	    FileUtils.copyFile(imagefile, imagefile1);	
	    return actualimagename;
	}
	
	@BeforeTest
	public void beforeTest() throws IOException
	{
		String browser = readFromPropertyFile("browser");
		launchBrower(browser);
		logger.info("launching "+browser+ " browser");
		String url = readFromPropertyFile("url");
		driver.get(url);
		logger.info("hiting url in browser");
		driver.manage().window().maximize();
		//waitHelper waithelper = new waitHelper(driver);
		//waithelper.setExplicitWait_waitUtilElementIsVisible(driver.findElement(By.xpath("//button[contains(text(),'Sign In')]")), 60);
		//waithelper.setExplicitWait_waitUtilElementIsVisible(driver.findElement(By.xpath("//button[contains(text(),'Sign In')]")));*/
	}
	
	@BeforeMethod
	public void beforeMethod(Method method)
	{
		extenttest = extentreport.startTest(method.getName(), "test started");
		extenttest.log(LogStatus.INFO, method.getName()+" test started");
		logger.info(" ");
		logger.info(method.getName()+ " :before method completed");
	}
	
	@AfterMethod
	public void afterMethod(ITestResult itestresult) throws IOException
	{
		if(itestresult.getStatus()==itestresult.SUCCESS)
		{
			extenttest.log(LogStatus.PASS, itestresult.getName()+" test pass");
			logger.info(itestresult.getName()+ " test is passed.");
		}
		
		else if(itestresult.getStatus()==itestresult.FAILURE)
		{
			extenttest.log(LogStatus.FAIL, itestresult.getName()+" test fail");
			extenttest.log(LogStatus.FAIL, extenttest.addScreenCapture(getScreenshot()));
			extenttest.log(LogStatus.FAIL, itestresult.getThrowable());
			logger.info(itestresult.getName()+ " test is failed.");
		}
		
		else if(itestresult.getStatus() == ITestResult.SKIP)
		{
			extenttest.log(LogStatus.SKIP, itestresult.getName()+" test is skipped");
			logger.info(itestresult.getName()+ " test is skipped.");
		}
		logger.info(itestresult.getName()+ " :after method completed");
	  }
	
	@AfterClass
	public void afterclass()
	{
		extentreport.endTest(extenttest);
		extentreport.flush();
		logger.info("after class completed");
	}
}
