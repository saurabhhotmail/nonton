package main.java.waithelper;

import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class waitHelper {

	WebDriver driver;
	Logger logger = Logger.getLogger(waitHelper.class);
	public waitHelper(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void setImplictitWait(long time,TimeUnit timeunit)
	{
		driver.manage().timeouts().implicitlyWait(time, timeunit);
		logger.info("implicit wait is set for: "+time+ " "+timeunit);
	}
	
	
	public void setExplicitWait_waitUtilElementIsVisible(WebElement element,long timeout)
	{
		WebDriverWait explicitWait = new WebDriverWait(driver,timeout);
		explicitWait.until(ExpectedConditions.visibilityOf(element));
		logger.info("explicit wait is set until element, "+element.getText() + ", is found");
	}
}
