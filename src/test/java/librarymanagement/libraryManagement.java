package test.java.librarymanagement;

import org.testng.annotations.Test;

import main.java.pageobject.Login;
import main.java.pageobject.Videos;
import main.java.testbase.testBase;

public class libraryManagement extends testBase {

	Login login;
	Videos videos;
	
	@Test(priority=0)
	public void loginToApplicatoin()
	{
		login = new Login(driver);
		login.enterEmail(pr.getProperty("email"));
		login.enterPassword(pr.getProperty("password"));
		login.clickSignInWithValidCredentials();
	}
	
	@Test(priority=1)
	public void verifyIfLibraryManagementDispaysOnLeftMenu()
	{
		videos = new Videos(driver);
		videos.verifyIfLibraryManagementDispaysOnLeftMenu();
	}
	
	@Test(priority=2)
	public void showEntetiesUnderLibraryManagement()
	{
		videos.showEntetiesUnderLibraryManagement();
	}
	
	@Test(priority=3)
	public void verifyIfVideosDisplaysUnderLibraryManagement()
	{
		videos.verifyIfVideosDisplaysUnderLibraryManagement();
	}
	
	@Test(priority=4)
	public void verifyNumberOfVideosDisplayingOnFirstPage()
	{
		videos.verifyNumberOfVideosDisplayingOnFirstPage();
	}
	
	@Test(priority=5)
	public void verifyTotalNumberOfVideos()
	{
		videos.verifyTotalNumberOfVideos();
	}
}
